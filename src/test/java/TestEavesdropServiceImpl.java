import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestEavesdropServiceImpl {

	EavesdropServiceImpl eavesdropService = null;
	
	@Before
	public void setUp() {
		eavesdropService = mock(EavesdropServiceImpl.class);
	}
	
	@Test
	public void testGetResponseFromEavesdrop() {
		
		when(eavesdropService.getResponseFromEavesdrop("_trove")).thenReturn("Correct!");
		String reply = eavesdropService.getResponseFromEavesdrop("_trove");
		assertEquals("Correct!", reply);
		verify(eavesdropService).getResponseFromEavesdrop("_trove");
		
	}
	
	@Test
	public void testReadDataFromEavesdrop() {
		
			URLConnection connection = mock(URLConnection.class);
			
			when(eavesdropService.readDataFromEavesdrop(connection)).thenReturn("Correct!");
			String result = eavesdropService.readDataFromEavesdrop(connection);			
			assertEquals("Correct!", result);		
			verify(eavesdropService).readDataFromEavesdrop(connection);
		
	}
	
	@Test
	public void testParseOutput() {
		
		String s = "This is a test input string.";
		
		List<String> linkNames = mock(List.class);
		
		when(eavesdropService.parseOutput(s)).thenReturn(linkNames);
		List<String> reply = eavesdropService.parseOutput(s);
		assertEquals(linkNames, reply);
		verify(eavesdropService).parseOutput(s);
		
	}
	
	@Test
	public void testGatherYearData() {
		
		List<String> mockProjectYears = mock(List.class);
		String baseURL = "http://eavesdrop.openstack.org/meetings/_trove";
		Map<String, List<String>> mockMap = mock(Map.class);
		
		when(eavesdropService.gatherYearData(mockProjectYears, baseURL)).thenReturn(mockMap); 
		Map<String, List<String>> result = eavesdropService.gatherYearData(mockProjectYears, baseURL);
		assertEquals(mockMap, result);			
		verify(eavesdropService).gatherYearData(mockProjectYears, baseURL);
			
	}

	@Test
	public void testProcessYearData() {

		Map<String, List<String>> filesMap = mock(Map.class);
		Map<String, Integer> countMap = mock(Map.class);
	
		when(eavesdropService.processYearData(filesMap)).thenReturn(countMap);
		Map<String, Integer> result = eavesdropService.processYearData(filesMap);		
		assertEquals(countMap, result);		
		verify(eavesdropService).processYearData(filesMap);
			
	}
	
	@Test
	public void testFormatYearData() {

		Map<String, Integer> countMap = mock(Map.class);
		
		when(eavesdropService.formatYearData(countMap)).thenReturn("Correct!");
		String result = eavesdropService.formatYearData(countMap);			
		assertEquals("Correct!", result);			
		verify(eavesdropService).formatYearData(countMap);
			
	}
}
