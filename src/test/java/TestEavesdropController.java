import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestEavesdropController {
	
	EavesdropController eavesdropController = new EavesdropController();
	EavesdropService mockEavesdrop = null;
	
	@Before
	public void setUp1() {		
		mockEavesdrop = mock(EavesdropService.class);		
		eavesdropController.setEavesdropService(mockEavesdrop);
	}
	
	@Test
	public void testIncorrectUsageRoot() {
		String reply = eavesdropController.incorrectUsageRoot();
		assertEquals("Usage: <br>http://localhost:8080/assignment3/meetings?project=&#60;project-name&#62;", reply);
	}
	
	@Test
	public void testIncorrectUsageMeetings() {
		String reply = eavesdropController.incorrectUsageMeetings();
		assertEquals("Usage: <br>http://localhost:8080/assignment3/meetings?project=&#60;project-name&#62;", reply);
	}

	@Test
	public void testGetProjectCorrectAction() {
		when(mockEavesdrop.getResponseFromEavesdrop("_trove")).thenReturn("");
		String reply = eavesdropController.getProject("_trove");
		assertEquals("Year&emsp;count", reply);
		verify(mockEavesdrop).getResponseFromEavesdrop("_trove");
	}
	
	@Test
	public void testGetProjectIncorrectAction() {
		when(mockEavesdrop.getResponseFromEavesdrop("nonexistantProjectName")).thenReturn("");
		String reply = eavesdropController.getProject("nonexistantProjectName");
		assertEquals("Unknown project nonexistantProjectName", reply);
		verify(mockEavesdrop, never()).getResponseFromEavesdrop("nonexistantProjectName");
	}
	
	@Test
	public void testGetProjectNullAction() {
		when(mockEavesdrop.getResponseFromEavesdrop(null)).thenReturn("");
		String reply = eavesdropController.getProject(null);
		assertEquals("Project name is null. Try again.", reply);
		verify(mockEavesdrop, never()).getResponseFromEavesdrop(null);
	}
}