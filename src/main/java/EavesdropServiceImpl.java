import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

// The service class should implement the logic of querying the eavesdrop site, parsing the data, and creating it for presentation.

public class EavesdropServiceImpl implements EavesdropService {

	String name = "EnglishEditor"; //xx
	URL eavesdropURL = null;
	String baseURL = "http://eavesdrop.openstack.org/";
	
	public EavesdropServiceImpl() {
		try {
			eavesdropURL = new URL(baseURL);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	// First method called by controller after error-checking projectName
	public String getResponseFromEavesdrop(String projectName) {
		String retVal = "";	
		baseURL = "http://eavesdrop.openstack.org/meetings/" + projectName;
			
		try {			
			eavesdropURL = new URL(baseURL);
			URLConnection connection = eavesdropURL.openConnection();
			
			// Call read and parse methods
			String readData = readDataFromEavesdrop(connection);
			List<String> projectYears = parseOutput(readData);
			Map<String, List<String>> filesMap = gatherYearData(projectYears, baseURL);
			Map<String, Integer> countMap = processYearData(filesMap);
			
			// Return 
			retVal = formatYearData(countMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return retVal;
	}
	
	public String readDataFromEavesdrop(URLConnection connection) {
		String retVal = "";
		try {
			BufferedReader in = new BufferedReader(
				new InputStreamReader(connection.getInputStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				retVal += inputLine;
			}
			in.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	// Insert all links from HTML input parameter into a List<>
	//call this in controller!!; perform error checking there but make these less repetitive
	public List<String> parseOutput(String inputString) {

		// Gather all links
		List<String> linkNames = new ArrayList<String>();
		Document doc = Jsoup.parse(inputString);
	    Elements links = doc.select("a");
	    
	    ListIterator<Element> iter = links.listIterator();
	    
	    // Skip header links (Name, Last Modified, Size, Description, Parent directory)
	    for(int i = 0; i < 5; i++)
	    {
	    	iter.next();
	    }
	    
	    // Insert link titles into
	    while(iter.hasNext()) {
	    		Element e = (Element) iter.next();
	    		String s = e.html();
	    		s = s.replace("#", "%23");
	    		s = s.substring(0, s.length()-1); // deletes ending '/'
	    		linkNames.add(s);
	    }

		return linkNames;
		
	}
	
	public Map<String, List<String>> gatherYearData(List<String> projectYears, String baseURL) {
		
		String url = "";
		Map<String, List<String>> filesMap = new LinkedHashMap<String, List<String>>();
		
		try {			
			
			// Visit all years and count their data. Can be broken into 2 methods.
			for(int i = 0; i < projectYears.size(); i++) {
				url = baseURL + "/" + projectYears.get(i);
				eavesdropURL = new URL(url);
				URLConnection connection = eavesdropURL.openConnection();
				String readData = readDataFromEavesdrop(connection);
				List<String> projectFiles = parseOutput(readData);
				filesMap.put(projectYears.get(i), projectFiles);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filesMap;
	}
	
	public Map<String, Integer> processYearData( Map<String, List<String>> filesMap) {

		List<String> projectFiles;
		Map<String, Integer> countMap = new LinkedHashMap<String, Integer>();
		
		for (String year : filesMap.keySet()) {
		    projectFiles = filesMap.get(year);
		    int count = 0;
		    // Iterate through the project files in search of *.log.html
		    for (String file : projectFiles) {
		    	if(file.contains(".log.htm")) { // TODO substring at parseOutput() removes 'l'. OK for now..
		    		count++;
		    	}
			}
		    
		    countMap.put(year, count);
		}

		return countMap;
	}
	
	public String formatYearData( Map<String, Integer> countMap) {
		String retVal = "";
		Integer count;
				
		for (String year : countMap.keySet()) {
		    count = countMap.get(year);
		    retVal = retVal + "<br>" + year + "&emsp;" + count;
		}
		
		return retVal;
	}

}
