import java.net.URLConnection;
import java.util.List;
import java.util.Map;


public interface EavesdropService {
		
	public String getResponseFromEavesdrop(String projectName);
	
	public String readDataFromEavesdrop(URLConnection connection);
	
	public List<String> parseOutput(String inputString);
	
	public Map<String, List<String>> gatherYearData(List<String> projectYears, String baseURL);
	
	public Map<String, Integer> processYearData( Map<String, List<String>> filesMap);
	
	public String formatYearData( Map<String, Integer> countMap);

}
