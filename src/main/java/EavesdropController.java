// The controller should perform error checking and error handling

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EavesdropController {

	
	private EavesdropService eavesdropService;
	
	
	public EavesdropController() {
		
	}
	
	
	public EavesdropController(EavesdropService eavesdropService) {
		this.eavesdropService = eavesdropService;
	}
	

	@ResponseBody
    @RequestMapping(value = "/")
    public String incorrectUsageRoot() {
        return "Usage: <br>http://localhost:8080/assignment3/meetings?project=&#60;project-name&#62;";
    }
	

	@ResponseBody
	@RequestMapping(value = "/meetings")
	public String incorrectUsageMeetings() {
		return "Usage: <br>http://localhost:8080/assignment3/meetings?project=&#60;project-name&#62;";
	}
	
	
	@ResponseBody
    @RequestMapping(value = "/meetings", params = {"project"}, method=RequestMethod.GET)
    public String getProject(@RequestParam("project") String project)
    {
		String ret = "";
		if (project != null) {
			
			// ensure project parameter exists on /meetings/
			try {
				// Connect to OpenStack and pull links
				List<String> projNames = new ArrayList<String>();
				String source = "http://eavesdrop.openstack.org/meetings";
				Document doc = Jsoup.connect(source).get();
	 		    Elements links = doc.select("a");
	 		    
	 		    ListIterator<Element> iter = links.listIterator();
	 		    
	 		    // Skip header links (Name, Last Modified, Size, Description, Parent directory)
	 		    for(int j = 0; j < 5; j++)
	 		    {
	 		    	iter.next();
	 		    }
	 		    
	 		    while(iter.hasNext()) {
			    		Element e = (Element) iter.next();
			    		String s = e.html();
			    		s = s.replace("#", "%23");
			    		s = s.substring(0, s.length()-1); // deletes ending '/'
			    		projNames.add(s);
	 		    }
	
	 		    if(projNames.contains(project)) {
	 		    	
	 		    	// Project name is valid, gather count data
	 				return ret = "Year&emsp;count" + eavesdropService.getResponseFromEavesdrop(project);
	 				
	 		    } else {
	 		    	
	 		    	// Invalid project parameter; does not exist at /meetings/
					return ret + "Unknown project " + project;
	 		    }
	 		    
			} catch(IOException exp) {
		 			exp.printStackTrace();
		 	}
			
		}
		return ret + "Project name is null. Try again.";
    }
	
	
	public void setEavesdropService(EavesdropService eavesdropService) {
		this.eavesdropService = eavesdropService;
	}
	
}